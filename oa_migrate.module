<?php
/**
 * @file
 * Module file for Open Atrium Migrate.
 */

/**
 * Implements hook_flush_caches().
 */
function oa_migrate_flush_caches() {
  global $conf, $databases;

  // We can optionally register the migrations automatically. This is great if
  // you don't need to do any configuration and this the only being migrated
  // here. If you need to do something more advanced, you're better off
  // creating a custom module and calling oa_migrate_register_migrations()
  // yourself.
  if (!empty($conf['oa_migrate_register_migrations'])) {
    foreach ($conf['oa_migrate_register_migrations'] as $name => $args) {
      $skip = FALSE;

      // Validate arguments before registering automatically.
      if (empty($args['database']) && !empty($databases[$args['database']])) {
        drupal_set_message(t('Not registering Open Atrium 2.x Migration! Update $conf["oa_migrate_register_migrations"]["@name"]["database"] in your settings.php to a valid database connection name and clear your cache to register.', array('@name' => $name)), 'error');
        $skip = TRUE;
      }
      elseif (empty($args['files_path']) && is_dir($args['files_path'])) {
        drupal_set_message(t('Not registering Open Atrium 2.x Migration! Update $conf["oa_migrate_register_migrations"]["@name"]["files_path"] in your settings.php to a valid directory path and clear your cache to register.', array('@name' => $name)), 'error');
        $skip = TRUE;
      }
      
      if (!$skip) {
        $args['suffix'] = !empty($args['suffix']) ? $args['suffix'] : str_replace('-', '_', "_$name");
        oa_migrate_register_migrations($args['database'], $args['files_path'], $args);
      }
    }
  }
}

/**
 * Register all migration classes.
 *
 * @param string $database
 *   The name of the database connection to import from. This should match
 *   one of the connections defined in the $databases in settings.php. See
 *   the 'Database configuration' documentation linked below.
 * @param string $files_path
 *   The absolute path to the top-level directory of the legacy site.
 * @param array $config
 *   (optional) An associative array with the following keys:
 *   - suffix: (string) A suffix to put after the 'machine_name' of every
 *   migration registered. This is important if you import multiple OA1 sites
 *   into a single OA2 site, each migration will need a seperate suffix so that
 *   the machine names of the migrations are unique.
 *   - name: (string) A human readable name for this migration. This will be
 *   used in the group title for in the Migrate UI.
 *   - parent_space_default: (integer) If specified, the NID of a Space to
 *   create imported spaces under by default. If not specified (and there is no
 *   'parent_space_map' entry either), then the Space will be created
 *   at the top-level.
 *   - parent_space_map: (array) If specified, an associative array mapping the
 *   OA1 Group NID to an OA2 Space NID to create the new Space under. If the
 *   group isn't in the map, 'parent_space_default' will be used or the Space
 *   will be created at the top-level.
 *   - skip_unknown_roles: (boolean) If set to TRUE, new roles won't be created
 *   on the OA2 site, only the default OA2 roles will be mapped from OA1.
 *   Defaults to FALSE.
 *   - source_keywords_vid: (int) The VID of the 'Keywords' taxonomy on the OA1
 *   site. If not specified, it will default to 2.
 *   - group_access: (int) If specified, this will force all newly created
 *   Spaces to have a particular 'group_access' value. Possible values include:
 *   0 = public group or 1 = private group.
 *   - show_space_menu: (boolean) If set to FALSE, it will hide the Space menu
 *   on newly created Spaces. The default is TRUE, which is to show the Space
 *   menu.
 *
 * @see https://www.drupal.org/node/310071
 */
function oa_migrate_register_migrations($database, $files_path, $config = array()) {
  global $conf;

  $suffix = !empty($config['suffix']) ? $config['suffix'] : '';
  $name = !empty($config['name']) ? $config['name'] : NULL;

  $group_name = 'oa_migrate' . $suffix;

  $arguments = array(
    '#arguments' => array(
      'source_connection' => $database,
      'source_version' => 6,
      'group_name' => $group_name,
      'format_mappings' => array(
        // 3 = Messaging plain text (shouldn't be used anywhere, but if it is)
        3 => 'plain_text',
        // 4 = Full HTML
        4 => 'full_html',
        // 5 = Markdown (TODO: do we enable markdown?)
        5 => 'panopoly_html_text',
        // 6 = Wysiwyg (only on BucketB Atrium)
        6 => 'panopoly_wysiwyg_text',
      ),
      'file_migration' => 'OaFile' . $suffix,
    ),
    'role' => array(
      'class_name' => 'OaRoleMigration',
      'machine_name' => 'OaRole' . $suffix,
      'description' => t('Import roles'),
      'skip_unknown_roles' => isset($config['skip_unknown_roles']) ? $config['skip_unknown_roles'] : FALSE,
      'role_mappings' => array(
        'manager' => 'editor',
        'administrator' => 'administrator',
      ),
    ),
    'picture' => array(
      'class_name' => 'DrupalPicture6Migration',
      'machine_name' => 'OaPicture' . $suffix,
      'description' => t('Import user pictures'),
      'default_uid' => 1,
      'source_dir' => $files_path,
    ),
    'user' => array(
      'class_name' => 'OaUserMigration',
      'description' => t('Import users'),
      'machine_name' => 'OaUser' . $suffix,
      'role_migration' => 'OaRole' . $suffix,
      'picture_migration' => 'OaPicture' . $suffix,
      'parent_space_default' => !empty($config['parent_space_default']) ? $config['parent_space_default'] : NULL,
    ),
    'file' => array(
      'class_name' => 'DrupalFile6Migration',
      'description' => t('Import files'),
      'machine_name' => 'OaFile' . $suffix,
      'user_migration' => 'OaUser' . $suffix,
      'default_uid' => 1,
      'source_dir' => $files_path,
    ),
    'taxonomy' => array(
      '#arguments' => array(
        'class_name' => 'DrupalTerm6Migration',
        // The soft dependency says that while we don't have to run the user migration
        // first, we want to make sure it's listed first so the vocubularies are
        // listed right ahead of the node migrations.
        'soft_dependencies' => array('OaUser' . $suffix),
      ),
    ),
    'node' => array(
      '#arguments' => array(
        'user_migration' => 'OaUser' . $suffix,
        'group_migration' => 'OaNodeGroup' . $suffix,
      ),
      'group' => array(
        'class_name' => 'OaNodeGroupMigration',
        'description' => t('Import Group nodes'),
        'machine_name' => 'OaNodeGroup' . $suffix,
        'source_type' => 'group',
        'destination_type' => 'oa_space',
        'parent_space_default' => !empty($config['parent_space_default']) ? $config['parent_space_default'] : NULL,
        'parent_space_map' => !empty($config['parent_space_map']) ? $config['parent_space_map'] : NULL,
        'group_access' => isset($config['group_access']) ? $config['group_access'] : NULL,
        'soft_dependencies' => array(
          'OaFile' . $suffix,
        ),
        'show_space_menu' => isset($config['show_space_menu']) ? $config['show_space_menu'] : TRUE,
      ),
      'wiki_section' => array(
        'class_name' => 'OaSectionMigration',
        'description' => t('Import Knowledge sections'),
        'machine_name' => 'OaSectionKnowledge' . $suffix,
        'source_type' => 'book',
        'destination_type' => OA_SECTION_TYPE,
        'section_settings' => array(
          'title' => 'Knowledge',
          'section_type' => 'News Section',
          'menu' => array(
            'link_title' => 'Knowledge',
            'hidden' => 1,
          ),
        ),
      ),
      'notebook' => array(
        'class_name' => 'OaNodeNotebookMigration',
        'description' => t('Import Notebook nodes'),
        'machine_name' => 'OaNodeNotebook' . $suffix,
        'source_type' => 'book',
        'destination_type' => 'oa_wiki_page',
        'soft_dependencies' => array(
          'OaFile' . $suffix,
        ),
        'dependencies' => array(
          'OaSectionKnowledge' . $suffix,
        ),
        'section_migration' => 'OaSectionKnowledge' . $suffix,
        'first_revision' => TRUE,
      ),
      'notebook_revisions' => array(
        'class_name' => 'OaNodeRevisionNotebookMigration',
        'description' => t('Import revisions for Notebook nodes'),
        'machine_name' => 'OaNodeRevisionNotebook' . $suffix,
        'source_type' => 'book',
        'destination_type' => 'oa_wiki_page',
        'dependencies' => array(
          'OaNodeNotebook' . $suffix,
        ),
        'node_migration' => 'OaNodeNotebook' . $suffix,
        'section_migration' => 'OaSectionKnowledge' . $suffix,
      ),
      'calendar_section' => array(
        'class_name' => 'OaSectionMigration',
        'description' => t('Import Calendar sections'),
        'machine_name' => 'OaSectionCalendar' . $suffix,
        'source_type' => 'event',
        'destination_type' => OA_SECTION_TYPE,
        'section_settings' => array(
          'title' => 'Calendar',
          'section_type' => 'Calendar Section',
          'menu' => array(
            'link_title' => 'Calendar',
            'hidden' => 1,
          ),
        ),
      ),
      'event' => array(
        'class_name' => 'OaNodeEventMigration',
        'description' => t('Import Event nodes'),
        'machine_name' => 'OaNodeEvent' . $suffix,
        'source_type' => 'event',
        'destination_type' => 'oa_event',
        'dependencies' => array(
          'OaSectionCalendar' . $suffix,
        ),
        'section_migration' => 'OaSectionCalendar' . $suffix,
      ),
      'discussion_section' => array(
        'class_name' => 'OaSectionMigration',
        'description' => t('Import Discussion sections'),
        'machine_name' => 'OaSectionDiscussion' . $suffix,
        'source_type' => 'blog',
        'destination_type' => OA_SECTION_TYPE,
        'section_settings' => array(
          'title' => 'Discussions',
          'section_type' => 'Discussion Section',
          'menu' => array(
            'link_title' => 'Discussions',
            'hidden' => 1,
          ),
        ),
      ),
      'discussion' => array(
        'class_name' => 'OaNodeDiscussionMigration',
        'description' => t('Import Discussion nodes'),
        'machine_name' => 'OaNodeDiscussion' . $suffix,
        'source_type' => 'blog',
        'destination_type' => 'oa_discussion_post',
        'soft_dependencies' => array(
          'OaFile' . $suffix,
        ),
        'dependencies' => array(
          'OaSectionDiscussion' . $suffix,
        ),
        'section_migration' => 'OaSectionDiscussion' . $suffix,
      ),
      'discussion_replies' => array(
        'class_name' => 'OaNodeDiscussionMigration',
        'description' => t('Import replies to Discussion nodes'),
        'machine_name' => 'OaNodeDiscussionReplies' . $suffix,
        'source_type' => 'blog',
        'destination_type' => 'oa_discussion_post',
        'dependencies' => array(
          'OaNodeDiscussion' . $suffix,
          'OaSectionDiscussion' . $suffix,
        ),
        'section_migration' => 'OaSectionDiscussion' . $suffix,
        'parent_migration' => 'OaNodeDiscussion' . $suffix,
      ),
    ),
    'menu' => array(
      '#arguments' => array(
        'group_migration' => 'OaNodeGroup' . $suffix,
        'node_migrations' => array(
          'OaNodeGroup' . $suffix,
          'OaNodeNotebook' . $suffix,
        ),
      ),
      'notebook' => array(
        'class_name' => 'OaMenuNotebookMigration',
        'description' => t('Import Notebook menu heirarchy'),
        'machine_name' => 'OaMenuNotebook' . $suffix,
        'notebook_migration' => 'OaNodeNotebook' . $suffix,
        'section_migration' => 'OaSectionKnowledge' . $suffix,
        'source_type' => 'book',
      ),
    ),
    'membership' => array(
      'class_name' => 'OaMembershipMigration',
      'description' => t('Import Group memberships'),
      'machine_name' => 'OaMembership' . $suffix,
      'user_migration' => 'OaUser' . $suffix,
      'group_migration' => 'OaNodeGroup' . $suffix,
    ),
    'notifications' => array(
      'class_name' => 'OaNotificationsMigration',
      'description' => t('Import notification subscriptions'),
      'machine_name' => 'OaNotifications' . $suffix,
      'user_migration' => 'OaUser' . $suffix,
      'node_migrations' => array(
        'OaNodeNotebook' . $suffix,
        'OaNodeEvent' . $suffix,
        'OaNodeDiscussion' . $suffix,
      ),
      'dependencies' => array(
        'OaUser' . $suffix,
        'OaNodeNotebook' . $suffix,
        'OaNodeEvent' . $suffix,
        'OaNodeDiscussion' . $suffix,
      ),
      'soft_dependencies' => array(
        'OaNodeDiscussionReplies' . $suffix,
      ),
    ),
  );

  // Only migrate the taxonomy if we have the keywords field available.
  if (module_exists('oa_migrate_legacy_keywords')) {
    // On a standard OA1 install, the 'keywords' taxonomy has the vid of 2.
    $source_keywords_vid = !empty($config['source_keywords_vid']) ? $config['source_keywords_vid'] : 2;

    $arguments['taxonomy']['keywords'] = array(
      'description' => t('Import keywords taxonomy'),
      'machine_name' => 'OaTaxonomyKeywords' . $suffix,
      'source_vocabulary' => $source_keywords_vid,
      'destination_vocabulary' => 'oa_migrate_legacy_keywords',
    );

    // Add the taxonomy soft dependency to all node migrations.
    foreach ($arguments['node'] as &$migration) {
      if (!isset($migration['soft_dependencies'])) {
        $migration['soft_dependencies'] = array();
      }
      $migration['soft_dependencies'][] = 'OaTaxonomyKeywords' . $suffix;
    }

    // Setup configuration about taxonomy to be used by all node migrations.
    $arguments['node']['#arguments']['source_keywords_vid'] = $source_keywords_vid;
    $arguments['node']['#arguments']['keywords_migration'] = 'OaTaxonomyKeywords' . $suffix;
  }

  // Allow other modules to customize it.
  $context = array_merge($config, array(
    'database' => $database,
    'files_path' => $files_path,
  ));
  drupal_alter('oa_migrate', $arguments, $context);

  // Register the group name in advance.
  MigrateGroup::register($group_name, $name);

  // Register all the migrations themselves.
  oa_migrate_generate($arguments);
}

/**
 *  Recursively processes specs to generate migrations.
 */
function oa_migrate_generate($specs, $arguments = array()) {
  if (isset($specs['#arguments'])) {
    $arguments = $arguments + $specs['#arguments'];
    unset($specs['#arguments']);
    foreach ($specs as $data) {
      oa_migrate_generate($data, $arguments);
    }
  }
  else {
    $arguments = $specs + $arguments;
    Migration::registerMigration($arguments['class_name'], $arguments['machine_name'], $arguments);
  }
}

/**
 * Implements hook_migrate_api().
 */
function oa_migrate_migrate_api() {
  $api = array(
    'api' => 2,
  );
  return $api;
}

/**
 * Implements hook_oa_messages_type_alter().
 */
function oa_migrate_oa_messages_type_alter(&$message_type, $context) {
  // Allow us to set a flag on entities to prevent the normal messages from
  // being created.
  if (!empty($context['entity']->oa_migrate_messages_skip)) {
    $message_type = NULL;
  }
}

/**
 * Implements hook_oa_messages_create_alter().
 */
function oa_migrate_oa_messages_create_alter($wrapper, $context) {
  // Try to ensure that the message date matches the date the changes were
  // actually made.
  //if ($wrapper->getBundle() == 'oa_create' && $context['entity_type'] == 'node') {
  if (in_array($wrapper->getBundle(), array('oa_create', 'oa_reply')) && $context['entity_type'] == 'node') {
    $wrapper->timestamp->set($context['entity']->created);
  }
  elseif ($wrapper->getBundle() == 'oa_update' && $context['entity_type'] == 'node') {
    $wrapper->timestamp->set($context['entity']->revision_timestamp);
  }
  elseif ($wrapper->getBundle() == 'oa_worktracker_task_update' && !empty($context['arguments']['comment'])) {
    $wrapper->timestamp->set($context['arguments']['comment']->created);
  }
}
